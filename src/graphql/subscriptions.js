// eslint-disable
// this is an auto generated file. This will be overwritten

export const onCreateSlide = `subscription OnCreateSlide {
  onCreateSlide {
    id
    slideTitle
    slideDescription
    orderNumber
    uri
  }
}
`;
export const onUpdateSlide = `subscription OnUpdateSlide {
  onUpdateSlide {
    id
    slideTitle
    slideDescription
    orderNumber
    uri
  }
}
`;
export const onDeleteSlide = `subscription OnDeleteSlide {
  onDeleteSlide {
    id
    slideTitle
    slideDescription
    orderNumber
    uri
  }
}
`;
