// eslint-disable
// this is an auto generated file. This will be overwritten

export const createSlide = `mutation CreateSlide($input: CreateSlideInput!) {
  createSlide(input: $input) {
    id
    slideTitle
    slideDescription
    orderNumber
    uri
  }
}
`;
export const updateSlide = `mutation UpdateSlide($input: UpdateSlideInput!) {
  updateSlide(input: $input) {
    id
    slideTitle
    slideDescription
    orderNumber
    uri
  }
}
`;
export const deleteSlide = `mutation DeleteSlide($input: DeleteSlideInput!) {
  deleteSlide(input: $input) {
    id
    slideTitle
    slideDescription
    orderNumber
    uri
  }
}
`;
