// eslint-disable
// this is an auto generated file. This will be overwritten

export const getSlide = `query GetSlide($id: ID!) {
  getSlide(id: $id) {
    id
    slideTitle
    slideDescription
    orderNumber
    uri
  }
}
`;
export const listSlides = `query ListSlides(
  $filter: ModelSlideFilterInput
  $limit: Int
  $nextToken: String
) {
  listSlides(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      slideTitle
      slideDescription
      orderNumber
      uri
    }
    nextToken
  }
}
`;
