import ApolloProvider from "react-apollo/ApolloProvider";
import SlideShowManager from "./SlideShowManager";
import {listSlides} from "../graphql/queries";
import ApolloClient from 'apollo-boost';
import SlideShow from "./SlideShow";
import {Query} from "react-apollo";
import gql from "graphql-tag";
import * as AOS from "aos";
import React from "react";


const client = new ApolloClient({
    uri: "https://zfv2ioc62vf6tmq54gdjm3d35y.appsync-api.eu-west-1.amazonaws.com/graphql",
    headers: {"X-Api-Key": "da2-6efsm5rynfbonfdtfxrvclkixa"}
});

AOS.init();

export default class App extends React.Component {

    render() {
        return (
            <ApolloProvider client={client}>
                <Query query={gql(listSlides)}>
                    {({loading, error, data}) => {
                        if (loading) return (
                            <div className="loading">Loading&#8230;</div>
                        );
                        if (error) return <h1>Error :(</h1>;

                        const slides = data.listSlides.items;


                        slides.sort(function (a, b) {
                            return a.orderNumber - b.orderNumber
                        });
                        return (
                            <React.Fragment>
                                <SlideShow slides={slides}/>
                                <SlideShowManager slides={slides}/>
                            </React.Fragment>
                        )
                    }}
                </Query>
            </ApolloProvider>
        )
    }
}