import {MOVE_DOWN, MOVE_UP} from "../graphqlQueries/queries";
import Mutation from "react-apollo/Mutation";
import React from "react";
import Modal from "react-bootstrap/Modal";
import {createSlide, deleteSlide, updateSlide} from "../graphql/mutations";
import gql from "graphql-tag";
import {listSlides} from "../graphql/queries";


export default class SlideShowManager extends React.Component {
    render() {
        return (
            <div className={"container"}>
                <h3>Slide show manager</h3>
                <SlideShowManagerNewSlide/>
                <SlideShowManagerCards slides={this.props.slides}/>
            </div>
        )
    }
}

class SlideShowManagerNewSlide extends React.Component {

    state = {
        slideTitle: 'new slide',
        slideDescription: 'new slide description',
        uri: "https://picsum.photos/1500/600"
    };


    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };


    render() {
        return (
            <React.Fragment>
                <div className={"card mb-2 mt-2"}>
                    <div className={"card-body"}>
                        <h3 className={"card-title"}>Create new slide</h3>
                    </div>
                </div>
                <div className={"card"}>
                    <div className={"card-body"}>
                        <form>
                            <div className={"row"}>
                                <div className={"col"}>
                                    <Form
                                        type={"text"}
                                        label={"Title"}
                                        handleChange={this.handleChange}
                                        name={"slideTitle"}
                                        value={this.state.slideTitle}
                                    />
                                </div>
                                <div className={"col"}>
                                    <Form
                                        type={"text"}
                                        label={"Description"}
                                        handleChange={this.handleChange}
                                        name={"slideDescription"}
                                        value={this.state.slideDescription}
                                    />
                                </div>
                            </div>
                            <div className={"row"}>
                                <div className={"col"}>
                                    <Form
                                        type={"text"}
                                        label={"URL"}
                                        handleChange={this.handleChange}
                                        name={"uri"}
                                        value={this.state.uri}
                                    />
                                </div>
                            </div>
                        </form>
                        <Mutation
                            mutation={gql(createSlide)}
                            variables={{
                                input: {
                                    slideDescription: this.state.slideDescription,
                                    slideTitle: this.state.slideTitle,
                                    uri: this.state.uri
                                }
                            }}
                            update={
                                (cache, {data: {createSlide}}) => {
                                    const data = cache.readQuery({query: gql(listSlides)});
                                    data.listSlides.items.unshift(createSlide);
                                    cache.writeQuery({query: gql(listSlides), data: data})
                                }
                            }
                            optimisticResponse={
                                {
                                    createSlide: {
                                        id: -1,
                                        slideDescription: this.state.slideDescription,
                                        slideTitle: this.state.slideTitle,
                                        orderNumber: 100500,
                                        uri: this.state.uri,
                                        __typename: "Slide"
                                    }
                                }
                            }
                        >
                            {pushFront => <button className={"btn btn-success"} onClick={pushFront}>Add to start</button>}
                        </Mutation>
                    </div>
                </div>
            </React.Fragment>
        );
    }

    static incrementOrderNumber(data) {
        for (let i = 0; i < data.listSlides.items.length; i++) {
            const slide = data.listSlides.items[i];
            slide.orderNumber += 1;
        }
    }
}

export const Form = (props) => {
    return (
        <div className="form-group">
            <label>{props.label}:</label>
            <input type={props.type} onChange={props.handleChange} className="form-control" name={props.name} value={props.value}/>
        </div>
    )
};

class SlideShowManagerCards extends React.Component {
    state = {
        show: false,
    };


    handleClose = () => {
        this.setState({show: false});
    };

    handleShow = () => {
        this.setState({show: true});
    };

    render() {
        const slides = this.props.slides;
        return (
            <React.Fragment>
                <div className={"card mb-2 mt-5"}>
                    <div className={"card-body"}>
                        <div className={"row"}>
                            <div className={"col"}>
                                <h3 className={"card-title"}>Slides manager</h3>
                            </div>
                            <div className={"col text-right"}>
                                <>
                                    <button className={"btn btn-info"} onClick={this.handleShow}>How to use?</button>

                                    <Modal show={this.state.show} onHide={this.handleClose}>
                                        <Modal.Header closeButton>
                                            <Modal.Title>How to use</Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                            <div className={"mr-2 ml-2"}>
                                                <div className={"row"}>
                                                    <button className={"btn btn-danger"}>DELETE</button>
                                                    <span className={"ml-2"}>click to delete the slide</span>
                                                    <div className={"alert alert-danger mt-2"}><strong>Warning!</strong> slide will be deleted
                                                        immediately without confirmation
                                                    </div>
                                                </div>
                                                <div className={"row"}>
                                                    <button className={"btn btn-warning"}>UPDATE</button>
                                                    <span className={"ml-2"}>enter new values and click to update the slide</span>
                                                    <div className={"alert alert-warning mt-2"}><strong>Warning!</strong> slide will be updated
                                                        immediately without confirmation
                                                    </div>
                                                </div>
                                                <div className={"row"}>
                                                    <span>
                                                    <button className={"btn btn-primary"}>⯅</button>
                                                    <button className={"btn btn-primary mr-2 ml-2"}>⯆</button>
                                                        <span>click to change slides order</span>
                                                    </span>
                                                </div>
                                            </div>
                                        </Modal.Body>
                                        <Modal.Footer>
                                            <button className={"btn btn-success"} onClick={this.handleClose}>Ok!</button>
                                        </Modal.Footer>
                                    </Modal>
                                </>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    slides.map((slide, key) =>
                        <div data-aos="fade-right" className={"card mb-2"} key={key}>
                            <div className={"card-body"}>
                                <SlideShowManagerCard slide={slide}/>
                            </div>
                        </div>
                    )
                }
            </React.Fragment>
        )
    }
}

class SlideShowManagerCard extends React.Component {

    state = {
        slideDescription: '',
        orderNumber: '',
        slideTitle: '',
        uri: ''
    };


    componentDidMount() {
        this.setState({
            slideDescription: this.props.slide.slideDescription,
            orderNumber: this.props.slide.orderNumber,
            slideTitle: this.props.slide.slideTitle,
            uri: this.props.slide.uri
        });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            slideDescription: nextProps.slide.slideDescription,
            orderNumber: nextProps.slide.orderNumber,
            slideTitle: nextProps.slide.slideTitle,
            uri: nextProps.slide.uri
        });
    }


    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };


    render() {
        const id = this.props.slide.id;
        const uri = this.state.uri;
        const title = this.state.slideTitle;
        const orderNumber = this.state.orderNumber;
        const description = this.state.slideDescription;

        return (
            <React.Fragment>
                <div className={"row"}>
                    <div className={"col"}>
                        <span className={"card-title"}>Slide ID: {
                            id === -1 ?
                                <div className="spinner-border spinner-border-sm" role="status">
                                    <span className="sr-only">Loading...</span>
                                </div> : id
                        }
                        </span>
                    </div>
                    <div className={"col text-right"}>
                        <Mutation
                            mutation={gql(deleteSlide)}
                            variables={{
                                input: {
                                    id: id
                                }
                            }}
                            update={
                                (cache, {data: {deleteSlide}}) => {
                                    const data = cache.readQuery({query: gql(listSlides)});
                                    SlideShowManagerCard.deleteSlideFromCache(data, id);
                                    cache.writeQuery({query: gql(listSlides), data: data});
                                }}
                        >
                            {deleteSlide => <button className={"btn btn-danger"} onClick={deleteSlide}>DELETE</button>}
                        </Mutation>
                        <Mutation
                            mutation={gql(updateSlide)}
                            variables={{
                                input: {
                                    id: id,
                                    slideTitle: title,
                                    slideDescription: description,
                                    uri: uri
                                }
                            }}
                            update={
                                (cache, {data: {updateSlide}}) => {
                                    const data = cache.readQuery({query: gql(listSlides)});
                                    SlideShowManagerCard.updateSlide(data, id, title, description, uri);
                                    cache.writeQuery({query: gql(listSlides), data: data});
                                }}
                            optimisticResponse={
                                {
                                    updateSlide: {
                                        id: id,
                                        slideTitle: title,
                                        slideDescription: description,
                                        uri: uri,
                                        __typename: "Slide"
                                    }
                                }
                            }

                        >
                            {updateSlide => <button className={"btn btn-warning ml-2 mr-2"} onClick={updateSlide}>UPDATE</button>}
                        </Mutation>
                        <Mutation
                            mutation={MOVE_DOWN}
                            variables={{
                                id: id
                            }}
                            update={
                                (cache, {data: {moveDown}}) => {
                                    const data = cache.readQuery({query: gql(listSlides)});
                                    SlideShowManagerCard.moveDown(data, id);
                                    cache.writeQuery({query: gql(listSlides), data: data});
                                }}
                            optimisticResponse={
                                {
                                    moveDown: {
                                        orderNumber: orderNumber + 1,
                                        slideDescription: description,
                                        slideTitle: title,
                                        id: id,
                                        uri: uri,
                                        __typename: "Slide"
                                    }
                                }
                            }
                        >
                            {moveDown => <button onClick={moveDown} className={"btn-sm btn btn-primary"}>⯆</button>}
                        </Mutation>
                        <Mutation
                            mutation={MOVE_UP}
                            variables={{
                                id: id
                            }}
                            update={
                                (cache, {data: {moveUp}}) => {
                                    const data = cache.readQuery({query: gql(listSlides)});
                                    SlideShowManagerCard.moveUp(data, id);
                                    cache.writeQuery({query: gql(listSlides), data: data});
                                }}
                            optimisticResponse={
                                {
                                    moveUp: {
                                        orderNumber: orderNumber - 1,
                                        slideDescription: description,
                                        slideTitle: title,
                                        id: id,
                                        uri: uri,
                                        __typename: "Slide"
                                    }
                                }
                            }
                        >
                            {moveUp => <button onClick={moveUp} className={"btn-sm ml-2 btn btn-primary"}>⯅</button>}
                        </Mutation>
                    </div>
                    <div className={"col-sm-1 text-right"}>
                        <h5># {orderNumber}</h5>
                    </div>
                </div>
                <div className={"row"}>
                    <div className={"col"}>
                        <label>Title:</label>
                        <input className={"form-control"} onChange={this.handleChange} name={"slideTitle"} value={title}/>
                    </div>
                    <div className={"col"}>
                        <label>Description:</label>
                        <input className={"form-control"} onChange={this.handleChange} name={"slideDescription"} value={description}/>
                    </div>
                </div>
                <label className={"mt-2 "}>Url:</label>
                <input className={"form-control"} onChange={this.handleChange} name={"uri"} value={uri}/>
            </React.Fragment>
        );
    }

    //List methods
    static moveUp(data, id) {
        const slide = SlideShowManagerCard.find(data, id);
        if (slide.orderNumber === 1) {
            return;
        }
        for (let j = 0; j < data.listSlides.items.length; j++) {
            const slideToIncrement = data.listSlides.items[j];
            if (slideToIncrement.orderNumber === slide.orderNumber - 1) {
                slideToIncrement.orderNumber += 1;
                break;
            }
        }
        slide.orderNumber -= 1;
    }

    static moveDown(data, id) {
        const slide = SlideShowManagerCard.find(data, id);
        if (slide.orderNumber === data.listSlides.items.length) {
            return;
        }
        for (let j = 0; j < data.listSlides.items.length; j++) {
            const slideToIncrement = data.listSlides.items[j];
            if (slideToIncrement.orderNumber === slide.orderNumber + 1) {
                slideToIncrement.orderNumber -= 1;
                break;
            }
        }
        slide.orderNumber += 1;
    }

    static updateSlide(data, id, title, description, uri) {
        const slide = SlideShowManagerCard.find(data, id);
        slide.slideTitle = title;
        slide.slideDescription = description;
        slide.uri = uri;
    }

    static deleteSlideFromCache(data, id) {
        for (let i = 0; i < data.listSlides.items.length; i++) {
            const slide = data.listSlides.items[i];
            if (slide.id === id) {
                const val = slide.orderNumber;
                SlideShowManagerCard.decrementOrderNumberFromVal(data, val);
                data.listSlides.items.splice(i, 1);
                break;
            }
        }
    }

    static decrementOrderNumberFromVal(data, val) {
        for (let j = 0; j < data.listSlides.items.length; j++) {
            const slide = data.listSlides.items[j];
            if (slide.orderNumber > val) {
                slide.orderNumber -= 1;
            }
        }
    }

    static find(data, id) {
        for (let i = 0; i < data.listSlides.items.length; i++) {
            const slide = data.listSlides.items[i];
            if (slide.id === id) {
                return slide;
            }
        }
    }
}